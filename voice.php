<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style_voice.css">
    <link rel="stylesheet" href="css/style_scanner.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type='text/javascript' src='js/voice.js'></script>
    <title>РазДельный Сбор - Создание обращения</title>
</head>
<body>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
     <div class="modal-dialog" role="document">
        <div class="modal-content">

        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Поднесите к камере штрих-код для считывания</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="QR-code">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="navbar-form navbar-right">
                        <select class="form-control" id="camera-select" style="display: none;"></select>
                        <div class="form-group">
                            <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                            <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span>Скан</button>
                            <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span> Начать сканирование </button>
                            <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span> Пауза</button>
                            <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span>Остановить</button>
                         </div>
                    </div>
                </div>
                
                <div class="panel-body text-center">
                    <div class="col-md-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="thumbnail" id="result">
                            <div class="well" style="display: none;">
                                <img width="320" height="240" id="scanned-img" src="">
                            </div> 
                        </div>
                    </div>
                </div>
              </div>  
              <div id="scanned-QR"></div>
            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="row navbar-brand" href="#"><img src="img/logo.svg" class="logo-img" alt="logo">РазДельный Сбор</a>
        <button class="navbar-toggler mr-5" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-around" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item" style="margin-left: 100px">
                    <a class="nav-link" href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item" style="margin-left: 325px">
                    <a class="nav-link" href="index.html">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="list.php">Обращения</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

    <div id = "form">
<form action="voice.php" method="post" enctype="multipart/form-data">
    <h2>Создать обращение</h2>
    <p>Оставьте обращение о трудноперерабатываемой упаковке, которое будет направлено производителю с просьбой заменить упаковку</p>

    <div class="row justify-content-center m-3">
                    <button type="button" class="btn btn-primary btn-block btn-round save" data-toggle="modal" data-target="#exampleModalLong">Сканер</button>
                </div>

    <a href=""><svg width="40" height="34" viewBox="0 0 40 34" fill="none" xmlns="http://www.w3.org/2000/svg" class="right">
<path d="M6.66667 7.00004H10V27H6.66667V7.00004ZM11.6667 7.00004H13.3333V27H11.6667V7.00004ZM15 7.00004H20V27H15V7.00004ZM21.6667 7.00004H23.3333V27H21.6667V7.00004ZM26.6667 7.00004H30V27H26.6667V7.00004ZM31.6667 7.00004H33.3333V27H31.6667V7.00004ZM3.33333 3.66671V10.3334H0V3.66671C0 2.78265 0.35119 1.93481 0.976311 1.30968C1.60143 0.684563 2.44928 0.333374 3.33333 0.333374H10V3.66671H3.33333ZM36.6667 0.333374C37.5507 0.333374 38.3986 0.684563 39.0237 1.30968C39.6488 1.93481 40 2.78265 40 3.66671V10.3334H36.6667V3.66671H30V0.333374H36.6667ZM3.33333 23.6667V30.3334H10V33.6667H3.33333C2.44928 33.6667 1.60143 33.3155 0.976311 32.6904C0.35119 32.0653 0 31.2174 0 30.3334V23.6667H3.33333ZM36.6667 30.3334V23.6667H40V30.3334C40 31.2174 39.6488 32.0653 39.0237 32.6904C38.3986 33.3155 37.5507 33.6667 36.6667 33.6667H30V30.3334H36.6667Z" fill="#333333"/>
</svg></a>

    <label for="vendor_code"> Артикул товара:<sup>*</sup></label>
    <input type="text" name="vendor_code" id="vendorCode" value="<?php echo $_POST['vendor_code']; ?>">
    <input type="submit" class="save" value="Проверить товар в системе" name="check_prod" id ="checking">

   
   
<?php 

    require_once 'connection.php'; // подключаем скрипт
    // подключаемся к серверу
    $link = mysqli_connect($host, $user, $password, $database) 
    or die("Ошибка " . mysqli_error($link));

    // выполняем операции с базой данных
    $query ="SELECT vendor_code FROM products WHERE vendor_code = ".$_POST["vendor_code"]."";
    $result = mysqli_query($link, $query); 
    if ($_POST["vendor_code"]) {
        if(!$result or !mysqli_num_rows($result))
            { 
                echo "<h4> В нашей базе пока такого товара нет! Вы первый! Заполните обращение: </h4> <br>";

                print '
                <label for="name_product">Наименование товара:<sup>*</sup></label>
                <input type="text" name="name_product" id="" >
            
                <a href="" class="right" id="add_brand">Добавить бренд</a>
            
                <label for="brand_product">Бренд:<sup>*</sup></label>
                <!-- <button id = "brand_another"> Добавить</button> -->
                <select name="brand_product" id="brand">
                <option value="1">Избенка</option>
                <option value="2">GreenFood</option>
                </select>
                <!-- <input type="hidden" name="brand_product" id="brand_input"> -->
                <a href="" class="right" id="add_man">Добавить производителя</a>
                <label for="manufacturer">Производитель:<sup>*</sup></label>
                <select name="manufacturer" id="">
                <option value="1">ВкусВилл</option>
                </select>
                <a href="" class="right" id="add_mat">Добавить материал</a>
                <label for="material">Материал упаковки:<sup>*</sup></label>
                <select name="material" id="">
                <option value="1">Твердый пластик небутылочный с маркировкой "1", PET, ПЭТ</option>
                <option value="2">Твердый пластик непрозрачный с маркировкой "1"/PET/ПЭТ</option>
                <option value="3">Твердый пластик с маркировкой "3", PVC или ПВХ</option>
                <option value="4">Твердый пластик с маркировкой "6", PS или ПС</option>
                <option value="5">Вспененный полистирол PS/ПС (подложки для овощей, фруктов, мяса)</option>
                <option value="6">Пластик с маркировкой "7";/Other</option>
                <option value="7">Комбинация пластиков (имеет буквенную или цифровую маркировку со знаком "С" или "/" . Например: C/HDPE, PP/LDPE)</option>
                <option value="8">Мягкий пластик (пакеты и пленка с любой маркировкой для круп, хлеба,упаковка для шоколада, конфет, этикетки и т.д.)</option>
                <option value="9">Tetra Pak и аналоги (“картонные” упаковки для молока, соков и т.д.)</option>
                <option value="10">Дой-пак (мягкая упаковка часто используется для майонезов и детского питания)</option>
                <option value="11">Тюбик (с любой маркировкой для зубной пасты, косметики и т.д.)</option>
                <option value="12">Пластик без маркировки</option>
                <option value="13">Маркировка на упаковке не соответствует действительности</option>
                <option value="14">Маркировка создает неясность для потребителя</option>
                </select>
                <div class="photo">
                <input type="file" name="photo_product" id="">
                <label for="photo_product">Добавить фото</label>
                </div>
                <label for="e-mail"> Email производителя:<sup>*</sup></label>
                <input type="email" name="e-mail" id="">
            
                <label for="text_voice">Введите текст обращения:</label>
                <textarea name="text_voice" id="" cols="30" rows="10"></textarea>
            
                <div class="photo">
                <input type="file" name="path" title="Выберите файл"/> </br>
                <label for="path">Прикрепить файл</label>
                </div>
            
                <label for="fio">Введите ФИО:<sup>*</sup></label>
                <input type="text" name="fio" id="" >
                <label for="region">Регион проживания:<sup>*</sup></label>
                <select name="region" id="">
                <option value="Москва">Москва</option>
                </select>
                <label for="email"> Введите Ваш email:<sup>*</sup></label>
                <input type="email" name="email" id="">
            
            <span><sup>*</sup> - Обязательное поле для заполнения</span>
                <div class="check">
                <input type="checkbox" name="check" id="">
                <label for="check">Согласен с политикой обработки персональных данных</label>
            </div>
            <input type="submit" name="button" value="Отправить" class="save">
                </form>
                </div>';
            } else {
                echo '<script>window.location = "thank.html";</script>';
            }
    }
    mysqli_close($link);
?>
  

<?php
$path = 'upload/'; // Ваш путь к дериктории

// Придумать имя для фалов, 
// можно по имени директории или имени пользователя, который аплодит, 
// что бы потом можно было легко ориентироваться
$fileNamePattern = 'Voice';
//Получить количество уже существующих файлов
if ($handle = opendir($path)) { 
    $counter = 0;
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $counter++;
        }
    }
    closedir($handle);
    // получить новое имя
    $newFileName  = $fileNamePattern.'-'.$counter;
}


 $file = "upload/".$_FILES['path']['name'];
 $photo = "img/".$_FILES['photo_product']['name'];

 $ext = end(explode('.', $file));
 $extph = end(explode('.', $photo));

 $filename = "upload/".$newFileName.".".$ext;
 $photoname = "img/".$newFileName.".".$extph;

  move_uploaded_file($_FILES['path']['tmp_name'], $filename);
  move_uploaded_file($_FILES['photo_product']['tmp_name'], $photoname);
 

 /* ----------------------------------------*/


// если были переданы данные для добавления в БД
if( isset($_POST['button']) && $_POST['button']== 'Отправить')
{
$mysqli = mysqli_connect('localhost', 'root', '', 'ecohack');
mysqli_query($mysqli, 'SET NAMES UTF8');
if( mysqli_connect_errno() ) // проверяем корректность подключения
echo 'Ошибка подключения к БД: '.mysqli_connect_error();
// формируем и выполняем SQL-запрос для добавления записи

$sql_res_prod=mysqli_query($mysqli, 'INSERT INTO products (`vendor_code`, `name`, `brand`, `manufacturer`, `material`, `photo`) VALUES (
     "'.htmlspecialchars($_POST['vendor_code']).'",
     "'.htmlspecialchars($_POST['name_product']).'",
     "'.htmlspecialchars($_POST['brand_product']).'",
     "'.htmlspecialchars($_POST['manufacturer']).'",
     "'.htmlspecialchars($_POST['material']).'",
     "'.htmlspecialchars($photoname).'")');

$product = mysqli_query($mysqli, 'SELECT * FROM products WHERE vendor_code = "'.htmlspecialchars($_POST['vendor_code']).'"');
while ($r_pr = $product->fetch_assoc()) {
    $id_product = $r_pr['id_product']."<br>";
}

$sql_res=mysqli_query($mysqli, 'INSERT INTO voice (`fio`, `email`, `region`, `text_voice`, `document`, `product`) VALUES (
    "'.htmlspecialchars($_POST['fio']).'",
    "'.htmlspecialchars($_POST['email']).'",
    "'.htmlspecialchars($_POST['region']).'",
    "'.htmlspecialchars($_POST['text_voice']).'",
    "'.htmlspecialchars($filename).'",
    "'.htmlspecialchars($id_product).'")'); // id_product 
// если при выполнении запроса произошла ошибка – выводим сообщение
if( mysqli_errno($mysqli) )
echo '<hr><div class="error col" style="color:red; font-size:20px">Запись не добавлена</div>'.mysqli_error($mysqli);
else // если все прошло нормально – выводим сообщение
echo '<hr><div class="ok col" style="color:green; font-size:20px">Запись добавлена</div>';


// несколько получателей
$to = 'gusevfedya@mail.ru, gusevfeodor@yandex.ru'; // обратите внимание на запятую

// тема письма
$subject = 'Birthday Reminders for August';

// текст письма
$message = '
<html>
<head>
  <title>Birthday Reminders for August</title>
</head>
<body>
  <p>Here are the birthdays upcoming in August!</p>
  <table>
    <tr>
      <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
    </tr>
    <tr>
      <td>Johny</td><td>10th</td><td>August</td><td>1970</td>
    </tr>
    <tr>
      <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
    </tr>
  </table>
</body>
</html>
';

// Для отправки HTML-письма должен быть установлен заголовок Content-type
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";


// Отправляем
mail($to, $subject, $message, implode("\r\n", $headers));

echo "<script>window.location = 'list.php';</script>";

}

?>  

</body>
<script type="text/javascript" src="js/filereader.js"></script>
        <script type="text/javascript" src="js/qrcodelib.js"></script>
        <script type="text/javascript" src="js/webcodecamjs.js"></script>
        <script type="text/javascript" src="js/main.js"></script>


<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>