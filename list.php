<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/list_style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <title>Список обращений</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="row navbar-brand" href="#"><img src="img/logo.svg" class="logo-img" alt="logo">РазДельный Сбор</a>
            <button class="navbar-toggler mr-5" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-around" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item" style="margin-left: 100px">
                        <a class="nav-link" href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item" style="margin-left: 325px">
                        <a class="nav-link" href="index.html">Главная</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="list.php">Обращения</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container my-4">
        <div class="row align-items-center justify-content-center">
            <h2 class="col-4 offset-md-4">Список обращений</h2>
            <button type="button" class="col-2 offset-md-2 btn btn-secondary btn-round"><a href="voice.php">Создать обращение</a></button>
        </div>
        <div class="row mt-5">
        <form method="post" action="list.php" style="width: 100%;">
            <div class="input-group mb-3">
                    <input type="text" class="form-control" name="search_q" placeholder="Поиск по наименованию товара" aria-label="Поиск по артиклу, наименованию товара, бренду, и по производителю" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-secondary btn-round-r px-4" type="submit">Поиск</button>
                    </div>
                </div>
            </form>
            <?php
                        require_once 'connection.php'; // подключаем скрипт
                    
                        // подключаемся к серверу
                        $link = mysqli_connect($host, $user, $password, $database) 
                            or die("Ошибка " . mysqli_error($link));

                        $search_q=$_POST['search_q'];
                        
                    if ($search_q) {
                        $search_q = trim($search_q);
                        $search_q = strip_tags($search_q);

                        $q= mysqli_query($link, "SELECT * FROM `products` WHERE name LIKE '%$search_q%'");
                        while ($r_p = $q->fetch_assoc()) {
                            $query = "SELECT * FROM `voice` WHERE product = ".$r_p["id_product"]."";
                        }
                    } else {
                        $query ="SELECT * FROM voice";
                    }
                    
                        
                    ?>


        </div>
    </div>
    <div class="container">
        <div class="row justify-content-around">
            <?php
                // выполняем операции с базой данных
                
                $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
                if($result)
                { 
                    while($row = mysqli_fetch_array($result)) { 
                        $article = preg_match("/^((\S+\s){5})/s", $row["text_voice"], $m) ? $m[1]: $row["text_voice"];

                        $query_row ="SELECT COUNT(*) FROM signatures WHERE voice =".$row["id_voice"]."";
                        $res_sign = mysqli_query($link, $query_row) or die("Ошибка " . mysqli_error($link)); 
                        $sign = 0;
                        while ($row_sign = $res_sign->fetch_assoc()) {
                            $sign = $row_sign['COUNT(*)'];
                        }

                        $query_prod ="SELECT * FROM products WHERE id_product = '".$row["product"]."'";
                        $res_prod = mysqli_query($link, $query_prod) or die("Ошибка " . mysqli_error($link)); 
                        while ($row_prod = $res_prod->fetch_assoc()) {
                            $product = $row_prod['name'];

                            $query_brand ="SELECT * FROM brand WHERE id_brand = '".$row_prod["brand"]."'";
                            $res_brand = mysqli_query($link, $query_brand) or die("Ошибка " . mysqli_error($link)); 
                            while ($row_brand = $res_brand->fetch_assoc()) {
                                $brand = $row_brand['brand_name'];
                            }

                            $query_man ="SELECT * FROM manufacturer WHERE id_manufacturer = '".$row_prod["manufacturer"]."'";
                            $res_man = mysqli_query($link, $query_man) or die("Ошибка " . mysqli_error($link)); 
                            while ($row_man = $res_man->fetch_assoc()) {
                                $manufacturer = $row_man['name'];
                            }

                            $query_mat ="SELECT * FROM material WHERE id_material = '".$row_prod["material"]."'";
                            $res_mat = mysqli_query($link, $query_mat) or die("Ошибка " . mysqli_error($link)); 
                            while ($row_mat = $res_mat->fetch_assoc()) {
                                $material = preg_match("/^((\S+\s){5})/s", $row_mat['title'], $m) ? $m[1]: $row_mat['title'];
                            }

                            $photo = $row_prod['photo'];
                        }
                        
                        print "  
                            <div class='col-3 m-2 my-4 p-3 eco-item'>
                                    <div class='row justify-content-end p-2 mx-1'>
                                        <p>
                                            <img src='img/view.svg' alt='views' class='view'><span> ".$sign."</span>
                                        </p>
                                    </div>
                                    <div class='row justify-content-center mb-2 box-photo'>
                                        <img src='".$photo."' alt='photo' class='photo'>
                                    </div>
                                    <p class='mx-3'><span class='text-secondary'>Наименование товара: </span>".$product."</p>
                                    <p class='mx-3'><span class='text-secondary'>Бренд: </span>".$brand."</p>
                                    <p class='mx-3'><span class='text-secondary'>Производитель: </span>".$manufacturer."</p>
                                    <p class='mx-3'><span class='text-secondary'>Материал упаковки: </span>".$material." ...</p>
                                    <p class='mx-3'><span class='text-secondary'>Обращение: </span> ".$article." ... </p>
                                    <p class='mx-3'>
                                        <img src='img/doc.svg' alt='doc' class='doc'> <u><a href='".$row['document']."' download='' title='Обращение'>Обращение.docx</a></u>
                                    </p>
                                    <div class='row justify-content-center m-3'>
                                        <button type='button' class='btn btn-primary btn-block btn-round' data-toggle='modal' data-target='#exampleModalLong'>Ознакомиться</button>
                                    </div>
                                    <!-- Modal -->
                                        <div class='modal fade' id='exampleModalLong' tabindex='-1' role='dialog' aria-labelledby='exampleModalLongTitle' aria-hidden='true'>
                                            <div class='modal-dialog' role='document'>
                                                <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <h5 class='modal-title' id='exampleModalLongTitle'>Текст обращения</h5>
                                                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                                    <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>
                                                <div class='modal-body'>
                                                    <p class='text-right'>Руководителю ООО ".$manufacturer."</p>
                                                    <p class='text-right'>От ".$row["fio"].",<br>регион: Московская область</p>
                                                    <p>".$row["text_voice"]."</p>
                                                </div>
                                                <div class='modal-footer'>
                                                    <button type='button' class='btn btn-secondary' data-dismiss='modal'>Закрыть</button>
                                                    <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal'>Подписать обращение</button>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal -->
                                    <div class='row justify-content-center m-3'>
                                        <button type='button' class='btn btn-secondary btn-round btn-block' data-toggle='modal' data-target='#exampleModal'>Подписать обращение</button>
                                    </div>

                                    <!-- Modal -->
                                    <div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                      <div class='modal-dialog' role='document'>
                                        <div class='modal-content'>
                                          <div class='modal-header'>
                                            <h5 class='modal-title' id='exampleModalLabel'>Подпись под коллективным обращением</h5>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                              <span aria-hidden='true'>&times;</span>
                                            </button>
                                          </div>
                                          <form action='' method='post'>
                                          
                                          <div class='modal-body'>
                                          <div id ='form'>

                                          


                                          <label for='fio'>ФИО:<sup>*</sup></label>
                                          <input type='text' name='fio' required >
                                          <label for='email'>Электронная почта:<sup>*</sup></label>
                                          <input type='email' name='email' required >
                                      
                                      <span><sup>*</sup> - Обязательное поле для заполнения</span>
                                          <div class='check'>
                                          <input type='checkbox' name='check' required>
                                          <label for='check' >Согласен с политикой обработки персональных данных</label>
                                          </div>
                                          



                                      
                                          </div>
                                          </div>
                                          <div class='modal-footer'>
                                            <input type='submit' id='buttom_sign' value='Подписать обращение'>
                                            
                                          </div>
                                          
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- Modal -->
                                </div>
                            ";
                    }
                   
                }
                
                // закрываем подключение
                mysqli_close($link);

            ?>
        </div>
    </div>
    <footer>
        <p class="text-center">
            2020 © Раздельный Сбор 
        </p>
    </footer>


</body>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>