-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 04 2020 г., 19:12
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ecohack`
--

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id_brand` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`id_brand`, `brand_name`) VALUES
(1, 'Избёнка'),
(2, 'GreenFood'),
(3, 'ЗдравСити');

-- --------------------------------------------------------

--
-- Структура таблицы `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `id_manufacturer` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `e-mail` varchar(45) NOT NULL,
  PRIMARY KEY (`id_manufacturer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `manufacturer`
--

INSERT INTO `manufacturer` (`id_manufacturer`, `name`, `address`, `e-mail`) VALUES
(1, 'ВкусВилл', 'г. Москва, улица Кулакова, д. 20, корп.1, эт. 10, пом. V, комн.1.', 'info@izbenka.msk.ru'),
(2, 'СпецМедЗащита', 'г. Санкт-Петербург, ул. Цветочная, д. 2', 'standart@specmed.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id_material` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `material`
--

INSERT INTO `material` (`id_material`, `title`) VALUES
(1, 'Твердый пластик небутылочный с маркировкой ''1'', PET, ПЭТ (лотки, контейнеры и\r\nпр.)'),
(2, 'Твердый пластик непрозрачный с маркировкой ''1''/PET/ПЭТ'),
(3, 'Твердый пластик с маркировкой ''3'', PVC или ПВХ'),
(4, 'Твердый пластик с маркировкой ''6'', PS или ПС'),
(5, 'Вспененный полистирол PS/ПС (подложки для овощей, фруктов, мяса)'),
(6, 'Пластик с маркировкой ''7'';/Other'),
(7, 'Комбинация пластиков (имеет буквенную или цифровую маркировку со знаком "С" или "/" . Например: C/HDPE, PP/LDPE)'),
(8, 'Мягкий пластик (пакеты и пленка с любой маркировкой для круп, хлеба,упаковка для шоколада, конфет, этикетки и т.д.)\r\n'),
(9, 'Tetra Pak и аналоги (“картонные” упаковки для молока, соков и т.д.)\r\n'),
(10, 'Дой-пак (мягкая упаковка часто используется для майонезов и детского питания)\r\n'),
(11, 'Тюбик (с любой маркировкой для зубной пасты, косметики и т.д.)'),
(12, 'Пластик без маркировки'),
(13, 'Маркировка на упаковке не соответствует действительности'),
(14, 'Маркировка создает неясность для потребителя');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_code` varchar(13) NOT NULL,
  `name` varchar(200) NOT NULL,
  `brand` int(11) NOT NULL,
  `manufacturer` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `photo` varchar(200) NOT NULL,
  PRIMARY KEY (`id_product`),
  KEY `idm` (`manufacturer`),
  KEY `idmat` (`material`),
  KEY `idbr` (`brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id_product`, `vendor_code`, `name`, `brand`, `manufacturer`, `material`, `photo`) VALUES
(1, '4680036750832', 'Вода газированная', 2, 1, 1, 'img/whater.jpg'),
(2, '4603728321912', 'Маски медицинские', 3, 2, 12, 'img/mask.jpg'),
(3, '4567512340970', 'Бананы в упаковке', 2, 1, 8, 'img/bananas.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `signatures`
--

CREATE TABLE IF NOT EXISTS `signatures` (
  `id_signature` int(11) NOT NULL AUTO_INCREMENT,
  `voice` int(11) NOT NULL,
  `FIO` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id_signature`),
  KEY `idvo` (`voice`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `signatures`
--

INSERT INTO `signatures` (`id_signature`, `voice`, `FIO`, `email`) VALUES
(1, 1, 'Козлов Эммануил Константинович ', 'kozema@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `voice`
--

CREATE TABLE IF NOT EXISTS `voice` (
  `id_voice` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(100) NOT NULL,
  `email` varchar(45) NOT NULL,
  `region` varchar(200) NOT NULL,
  `text_voice` varchar(500) NOT NULL,
  `document` varchar(200) NOT NULL,
  `product` int(11) NOT NULL,
  PRIMARY KEY (`id_voice`),
  KEY `idpr` (`product`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `voice`
--

INSERT INTO `voice` (`id_voice`, `fio`, `email`, `region`, `text_voice`, `document`, `product`) VALUES
(1, 'Иванов Иван Иванович', 'ivanov@mail.ru', 'Астраханская обл.', 'Прошу вас заменить пластиковую бутылку вашего молока на аналог TetraPack или картонную упаковку', 'upload/Voice-1.docx', 1),
(2, 'Антоненко Сергей Сергеевич', 'antserg@yandex.ru', 'Ленинградская обл.', 'Уважаемый производитель! Прошу вас сменить упаковку такого популярного на сегодняшний день товара!', 'upload/Voice-2.docx', 2),
(3, 'Николаенко Елена Алексеевна', 'niko@gmail.com', 'Московская обл.', 'Призываю всех поставщиков бананов перестать продавать продукты в полиэтиленовой упаковке!', 'upload/Voice-3.docx', 3);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `idbr` FOREIGN KEY (`brand`) REFERENCES `brand` (`id_brand`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idm` FOREIGN KEY (`manufacturer`) REFERENCES `manufacturer` (`id_manufacturer`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idmat` FOREIGN KEY (`material`) REFERENCES `material` (`id_material`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `signatures`
--
ALTER TABLE `signatures`
  ADD CONSTRAINT `idvo` FOREIGN KEY (`voice`) REFERENCES `voice` (`id_voice`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `voice`
--
ALTER TABLE `voice`
  ADD CONSTRAINT `idpr` FOREIGN KEY (`product`) REFERENCES `products` (`id_product`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
